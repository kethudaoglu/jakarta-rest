/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.learning.phonebook.bean;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import tr.learning.phonebook.entity.Person;
import tr.learning.phonebook.repository.RepositoryPerson;

/**
 * RequestScope, SessionScope, ConversationScope, Depended
 * @author alperen
 */
@ViewScoped
@Named
public class BeanPerson implements Serializable{
    
    @EJB
    //@Inject
    private RepositoryPerson repositoryPerson;
    
    
    @Getter @Setter
    private Person selectedPerson =new Person();
    
    
    @PostConstruct
    private void init(){
     
    }
    
    public void save(){
        repositoryPerson.add(selectedPerson);
       
    }
    
    
}
