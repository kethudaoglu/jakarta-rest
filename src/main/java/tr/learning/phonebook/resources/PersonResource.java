/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.learning.phonebook.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import tr.learning.phonebook.entity.Person;
import tr.learning.phonebook.repository.RepositoryPerson;

/**
 *
 * @author alperen
 */
@Path("persons")
public class PersonResource {

    @Inject
    private RepositoryPerson repositoryPerson;
    @Context
    private UriInfo uriInfo;

    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response persons(@Context HttpServletRequest request) {
        
        if (!request.getRemoteAddr().equals("0:0:0:0:0:0:0:1")) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok(repositoryPerson.getPersons().stream().collect(Collectors.toList())).build();
    }

    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    @Path("/{name}")
    public Response persons(@PathParam(value = "name") String name) {
        return Response.ok(repositoryPerson.getPersons().stream().filter(f -> f.getName().contains(name)).collect(Collectors.toList())).build();
    }

    @POST
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response addPerson(Person person) {
        repositoryPerson.add(person);
        return Response.status(Response.Status.CREATED).entity(repositoryPerson.getPersons()).build();
    }

    @DELETE
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response removePerson(@QueryParam(value = "identityNumber") String identityNumber) {
        repositoryPerson.getPersons().remove(new Person(identityNumber));
        return Response.ok(repositoryPerson.getPersons()).build();
    }

    @PUT
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response updatePerson(Person person) {
        repositoryPerson.update(person);
        return Response.ok(repositoryPerson.getPersons()).build();
    }
;
}
