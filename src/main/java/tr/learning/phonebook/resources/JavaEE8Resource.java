package tr.learning.phonebook.resources;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import tr.learning.phonebook.repository.RepositoryPerson;

/**
 *
 * @author 
 */
@Path("javaee8")
public class JavaEE8Resource {
    
    @EJB
    private RepositoryPerson repositoryPerson;
    @GET
    public Response ping(){
        return Response
                .ok("ping")
                .build();
    }
   
//    @GET
//    @Path("/person")
//    @Consumes(value = { MediaType.APPLICATION_JSON})
//    @Produces(value = {MediaType.APPLICATION_JSON})
//    public List<Person> listAll(){
//        return repositoryPerson.getPersons().stream().collect(Collectors.toList());
//    }
}
