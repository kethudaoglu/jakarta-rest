/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.learning.phonebook.repository;

import java.util.Date;
import java.util.HashSet;
import tr.learning.phonebook.entity.Person;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Named;

;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author alperen
 */


@Startup
@Singleton
@Getter
@Setter
@Named
public class RepositoryPerson {

    private Set<Person> persons;

    @PostConstruct
    private void init() {
        System.out.println("PstConstroct is called" + this.getClass().getName());
        persons = new HashSet<>();
        persons.add(new Person("10496638122", "Adnan", "BİLGEN", 44,new Date()));
        persons.add(new Person("10496638120", "Alperen", "KETHUDAOĞLU", 40,new Date()));
    }

    public void add(Person p) {
        persons.add(p);
    }

    public void update(final Person p){
        persons.remove(p);
        persons.add(p);
        
                
        
    }

    @PreDestroy
    private void destroy() {
        System.out.println("PreDestroy is called" + this.getClass().getName());
    }

}
