/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.learning.phonebook.filter;

/**
 *
 * @author alperen
 */
public interface IArrayList <T>{
    boolean add(T t);
    boolean clear();
    boolean contains(T t);
    T get(int i);
    int indexOf(T t);
    boolean remove(T t);
    default Object[] createO(int capacity){
        return new Object[capacity];
    }
    int size();
}
