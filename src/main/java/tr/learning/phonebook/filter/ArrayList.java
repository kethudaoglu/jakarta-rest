/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.learning.phonebook.filter;

import java.util.Arrays;
import java.util.Collection;

/**
 *
 * @author alperen
 * @param <T>
 */
public class ArrayList<T> implements IArrayList<T> {

    private static final int DEFAULT_CAPACITY = 10;
    private transient Object[] tempO;
    private int initialCapacity = DEFAULT_CAPACITY;
    private int size = 0;

    Object[] o;

    public ArrayList() {
        o = new Object[DEFAULT_CAPACITY];
    }

    public ArrayList(int initialCapacity) {

        o = new Object[initialCapacity];
    }

    public ArrayList(Collection<? extends T> c) {
        
        o = c.toArray();
        if ((size = o.length) != 0) {
            // c.toArray might (incorrectly) not return Object[] (see 6260652)
            if (o.getClass() != Object[].class)
                o = Arrays.copyOf(o, size, Object[].class);
        } else {
            // replace with empty array.
            this.o = this.createO(DEFAULT_CAPACITY);
        }

    }

    @Override
    public boolean add(T t) {

        Boolean statu = false;
        if (size < initialCapacity) {
            o[size++] = t;
            statu = true;
        } else {
            initialCapacity=DEFAULT_CAPACITY + size;
            Object[] oTemp = this.createO(initialCapacity);
            int i = 0;
            for (Object object : o) {
                oTemp[i++] = object;
            }
            size = i;
            o = oTemp;
            add(t);

        }
        return statu;
    }

    @Override
    public boolean clear() {
        o = new Object[initialCapacity];
        size = 0;
        return true;
    }

    @Override
    public boolean contains(T t) {
        int i = 0;
        do {
            if (o[i].equals(t)) {
                return true;
            }
            i++;
        } while (i < o.length);
        return false;

    }

    @Override
    public T get(int i) {
        return (T) o[i];
    }

    @Override
    public int indexOf(T t) {
        int i = 0;
        do {
            if (o[i].equals(t)) {
                return i;
            }
            i++;
        } while (i < o.length);
        return -1;
    }

    @Override
    public boolean remove(T t) {
        int i = 0;
        int c = 0;
        boolean statu = false;
        tempO = this.createO(initialCapacity);
        do {
            if (!o[i].equals(t)) {
                tempO[c] = o[i];
                c++;
            } else {
                statu = true;
                size--;
            }
            i++;
        } while (i < size);
        o = tempO;

        return statu;
    }

    @Override
    public int size() {
        return size;
    }

}
