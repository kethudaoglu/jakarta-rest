/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.learning.phonebook.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author alperen
 */
@Provider
public class RestFilter implements ContainerResponseFilter,ContainerRequestFilter {

    private static final String KEY_AUTHORIZATION_HEADER="Authorization";
    private static final String KEY_AUTHORIZATION_PREFIX="Basic";
    
    @Context
    private HttpServletRequest servletRequest;

    @Override
    public void filter(ContainerRequestContext crc, ContainerResponseContext crc1) throws IOException {
        System.out.println("in Response Filter ");
        if (servletRequest.getRemoteAddr().equals("0:0:0:0:0:0:0:1")) {
            System.out.println("Equal at Response Filter");
        } else {
            System.out.println("Not Equal at Response Filter");
        }
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        
        if (!requestContext.getUriInfo().getPath().contains("secure")){
            return;
        }
        
        ArrayList list;
        List<String> authHeader=requestContext.getHeaders().get(KEY_AUTHORIZATION_HEADER);
        if (authHeader!=null && !authHeader.isEmpty()){
            String token=authHeader.get(0).replaceFirst(KEY_AUTHORIZATION_PREFIX, "");
            String decodedString=Base64.getDecoder().decode(token).toString();
            StringTokenizer tokenizer=new StringTokenizer(decodedString);
            String username=tokenizer.nextToken();
            String password=tokenizer.nextToken();
            
            if ("user".equals(username) && "password".equals(password)){
                return;
            }
            
        }
        
        Response unauthorizedResponse=Response.status(Response.Status.UNAUTHORIZED).entity("user can not access the resource").build();
        requestContext.abortWith(unauthorizedResponse);
        
    }

}

