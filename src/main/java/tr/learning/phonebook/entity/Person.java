/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.learning.phonebook.entity;

import java.util.Date;
import javax.json.bind.annotation.JsonbDateFormat;
import javax.json.bind.annotation.JsonbProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 *
 * @author alperen
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"name","surname","age"})
public class Person {
    @JsonbProperty("identity-number")
    private String identityNumber;
    private String name;
    private String surname;
    private Integer age;
    @JsonbDateFormat(value = "dd-MM-yyyy")
    private Date dateOfBirth=new Date();
    
    public Person(String identityNumber){
        this.identityNumber=identityNumber;
    }
}
